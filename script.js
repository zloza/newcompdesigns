$(document).ready(function(){
    
    function spin(x) {
        var angle = 0;
        var k = 0;
        var j = 1;
        var int = setInterval(function() {
        if (k < x-1) k++;
        else { clearInterval(int);}
        angle += 90;
        $('.c1').css('transform', 'rotateX('+angle+'deg) translateZ(50px)');
        $('.c2').css('transform', 'rotateX('+(angle+270)+'deg) translateZ(50px)');
        $('.c3').css('transform', 'rotateX('+(angle+180)+'deg) translateZ(50px)');
        $('.c4').css('transform', 'rotateX('+(angle+90)+'deg) translateZ(50px)');
        
        $('.c1').css('opacity', '0');
        $('.c2').css('opacity', '0');
        $('.c3').css('opacity', '0');
        $('.c4').css('opacity', '0');
        
            j++;
        if (j == 5) j -= 4;
        $('.c'+j).css('opacity', '1');
        
    },1200);
        
        $('.design').delay(4400).animate({'top': '+=80px', 'opacity': '1'},700);
        $('.develop').delay(4300).animate({'top': '+=130px', 'opacity': '1'},600);
        $('.scroll').delay(4700).animate({'opacity': 1},1200);
        
        
        
    }
    
    spin(3);
    
    
    $("#hamburger" ).click(function(){
        $("#hamburger").toggleClass( "open" );
        $("#wrap-all").toggleClass( "slide" );
        $("header").toggleClass( "slide" );
    });
    
        $(".scroll" ).click(function(){
            $('html, body').animate({
           'scrollTop':   $('#portfolio').offset().top - 200
         }, 800);
        });
        $("#top-nav" ).click(function(){
            $('html, body').animate({
           'scrollTop':   $('#home').offset().top
         }, 800);
        });
        $("#portfolio-nav" ).click(function(){
            $('html, body').animate({
           'scrollTop':   $('#portfolio').offset().top - 200
         }, 800);
        });
        $("#about-nav" ).click(function(){
            $('html, body').animate({
           'scrollTop':   $('#about').offset().top - 80
         }, 800);
        });
        $("#contact-nav" ).click(function(){
            $('html, body').animate({
           'scrollTop':   $('#contact').offset().top
         }, 800);
        });
    
});